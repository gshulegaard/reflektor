# -*- coding: utf-8 -*-


__title__ = "reflektor"
__summary__ = "Static website generator application built around lektor"
__url__ = "https://gitlab.com/gshulegaard/reflektor"
__author__ = "Grant Hulegaard"

__version__ = "0.1.1"

__license__ = ""
__copyright__ = "Copyright Grant Hulegaard"

__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


__all__ = []

