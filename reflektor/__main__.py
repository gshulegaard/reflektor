# -*- coding: utf-8 -*-


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


# if env is dev...
import os
if os.environ.get('REFLEKTOR_ENV') == 'development':
    # add reflektor to sys.path
    import sys
    sys.path[0] = os.getcwd()


# A quick shim to run the CLI when running from the top module
from reflektor.cli.main import main
main()

