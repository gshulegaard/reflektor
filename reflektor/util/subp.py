# -*- coding: utf-8 -*-
import subprocess

from reflektor.common.error import ReflektorError


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


class ReflektorSubpError(ReflektorError):
    description = "An error occurred during subprocess.call"


def call(command, stdin=None, check=True, raw=False, fg=False):
    """
    Calls subprocess.Popen with the command

    :command: full shell command
    :stdin: stdin to passthrough via communicate
    :check: check the return code or not
    :raw: flag to return raw outputs or not
    :return: retcode int, stdout [], stderr []
    """
    subprocess_params = dict(
        shell=True,
    )

    if not fg:
        subprocess_params.update(dict(
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        ))

    if stdin:
        if 'stdin' in subprocess_params:
            del subprocess_params['stdin']
        subprocess_params['input'] = stdin

    process = subprocess.run(command, **subprocess_params)
    try:
        if process.returncode != 0 and check:
            raise ReflektorSubpError(
                message=command,
                payload=dict(returncode=process.returncode)
            )
        elif fg:
            return process.returncode, None, None
        elif raw:
            return process.returncode, process.stdout, process.stderr
        else:
            # out, err are bytes objects
            out = process.stdout.decode().split('\n')
            err = process.stderr.decode().split('\n')
            return process.returncode, out, err
    except:
        raise

