# -*- coding: utf-8 -*-


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


class ReflektorError(Exception):
    """
    Simple base exception to provide boiler plate for readable, consistent
    error messages.  Also has a dict representation for easy(ish) conversion to
    JSON for web use cases.

    Provides some basic extensions and representation handling.
    """

    description = 'Something unexpected happened'

    def __init__(self, message=None, payload=None):
        self.message = message if message is not None else self.description
        self.payload = payload
        super().__init__(self.message)

    @property
    def name(self):
        return self.__class__.__name__

    @property
    def dict(self):
        return {
            'error': self.name,
            'message': self.message,
            'payload': self.payload if self.payload is not None else {}
        }

    def __repr__(self):
        return "{0!s} {1!s}".format(self.message, self.payload)

    def __str__(self):
        return self.__repr__()

