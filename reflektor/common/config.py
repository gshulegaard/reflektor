# -*- coding: utf-8 -*-
import yaml

from reflektor.util.singleton import Singleton


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


class ReflektorConfig(Singleton):
    # default path, can be overridden during init
    path = '/etc/reflektor/'

    # default config, overriden by values loaded from file
    config = {
        "git": {
            "auth": "token",
            "credentials": ""
        },
        "nginx": {
            "listen": ":5001",
            "server_name": "",
        },
        "admin": {
            "enabled": True,
            "auth": "basic"
        }
    }

    # placeholder for logging_config, loaded during init
    logging_config = {}

    def __init__(self, path=None):
        """
        Init is responsible for setting the config path as well doing an initial
        load and save of the config.
        """
        # Skip init if Singleton flag is not set
        if not self._init:
            return

        super().__init__()  # flip the init flag for future

        # save the path if provided
        if path is not None:
            self.path = path

        self.logger_path = self.path.split('/', -1)[0] + '/logger.yaml'

        self.load()

    def apply(self, patch):
        """
        Applies changes to in memory config.

        :param patch: Dict Python YAML object of changes to apply.
        """
        self.config = {**self.config, **patch}

    def load(self):
        """
        Load the config and apply the vars to the in memory config.
        """
        # if path is None, skip loading
        if self.path is None:
            return

        with open(self.path, 'r') as f:
            loaded_config = yaml.load(f)

        # apply config from file to the in memory default
        self.apply(loaded_config)

        # load and just save logging config
        with open(self.logger_path, 'r') as f:
            self.logging_config = yaml.load(f)

    def save(self):
        """
        Save the current config to the filesystem.
        """
        with open(self.path, 'w') as f:
            yaml.dump(self.config, f, default_flow_style=False)

    def show(self):
        """
        YAML dump the config.
        """
        return yaml.dump(self.config, default_flow_style=False)

    def get(self, key, default=None):
        return self.config.get(key, default)

    def __getitem__(self, item):
        return self.config[item]

    def __setitem__(self, item, value):
        self.config[item] = value

