# -*- coding: utf-8 -*-
import logging

from logging import handlers
from logging import config

__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


LOGGERS_CACHE = {}


class ReflektorLogRecord(logging.LogRecord):
    def __init__(self, *args, **kwargs):
        from reflektor.context import context
        super().__init__(*args, **kwargs)
        self.action_id = context.action_id
        self.action_duration = '{:.3f}'.format(context.action_duration)


class ReflektorLogger(logging.getLoggerClass()):
    def makeRecord(
        self, name, level, fn, lno, msg, args, exc_info, func=None,
        extra=None, sinfo=None
    ):
        return ReflektorLogRecord(
            name, level, fn, lno, msg, args, exc_info
        )


# Loading custom handlers is funky in because Python3 removes getattr() module
# walking in favor of importlib.  The builtin logging library however still
# uses getattr() module walks so using custom handlers currently doesn't work.
class WatchedFileHandler(handlers.WatchedFileHandler):
    """
    Modification of standard WatchedFileHandler which is able
    to reload streams if needed
    """
    def reload(self):
        if self.stream is not None:
            # we have an open file handle, clean it up
            self.stream.flush()
            self.stream.close()
            self.stream = None

            # open a new file handle and get new stat info from that fd
            self.stream = self._open()
            self._statstream()


logging.setLoggerClass(ReflektorLogger)


def setup(logger_config):
    """
    Reads config from all logger.py config files and setups logging config.
    """
    config.dictConfig(logger_config)


def get(log_name):
    """
    Creates logger object to specified log and caches it in LOGGERS_CACHE dict.

    :param log_name: String log name
    :return: logger object
    """
    if log_name not in LOGGERS_CACHE:
        logger = logging.getLogger(log_name)
        LOGGERS_CACHE[log_name] = logger
    return LOGGERS_CACHE[log_name]

