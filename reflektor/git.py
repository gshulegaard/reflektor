# -*- coding: utf-8 -*-
from reflektor.context import context
from reflektor.util import subp


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


PATH = "./reflektor-src"


def _format_url():
    template = '{proto!s}://{credentials!s}@{repo!s}'
    proto = 'https'

    repo = context.config['git']['repo']
    auth = context.config['git']['auth'].lower()
    credentials = context.config['git']['credentials']

    # do some credential massage if needed
    if auth == 'token':
        credentials = 'oauth2:{!s}'.format(credentials)
    elif auth == 'ssh':
        credentials = 'git'
        proto = 'ssh'
    elif auth == 'basic':
        # if basic is selected, but credentials were blank: use default
        if not credentials:
            credentials = 'git'

    return template.format(proto=proto, credentials=credentials, repo=repo)


def clone():
    """
    Clones the target repositiory into the CWD as "./reflektor-src".
    """
    subp.call('git clone {0!s} {1!s}'.format(_format_url(), PATH))


def remove():
    """
    Removes the cloned repository (if it exists).
    """
    subp.call('rm -rf {!s}'.format(PATH), check=False)


def set_remote_origin():
    """
    Sets the remote origin to avoid having to continue to enter/manage
    credentials.
    """
    subp.call(
        'cd {0!s} && git remote set-url origin {1!s}'.format(
            PATH, _format_url()
        )
    )


def pull():
    """
    Update the local repo from the remote origin.

    There iis one thing special about this PULL method, it uses the "recursive"
    strategy explicity and also defaults conflicts to local changes using the
    option ("-X") "ours".
    """
    subp.call(
        'cd {!s} && git pull -s recursive -X ours'.format(PATH),
        check=False
    )

    # compute how many directories we walked down
    dir_count = PATH.count('/')
    if not dir_count:
        dir_count = 1

    # walk back to the original directory
    subp.call('cd {!s}'.format('../' * dir_count), check=False)

