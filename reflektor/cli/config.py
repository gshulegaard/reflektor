# -*- coding: utf-8 -*-
import click

from reflektor.context import context
from reflektor.cli import ReflektorCLIError


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


@click.group(short_help="Configure reflektor.")
def config():
    """
    Configure reflektor

    This command allows you set individual config values or edit the reflektor
    config file directly.
    """
    pass


@config.command(short_help="Set a single config value.")
@click.argument('key')
@click.argument('value')
def set(keys, value):
    """
    Set a config value.

    Use "." to access nested config values.  For example:

    \b
        reflektor config set git.repo gitlab.com/repo.git

    Allows you to set the "config['git']['repo']" value to
    "gitlab.com/repo.git".
    """
    keys = key.split('.')
    keys_copy = keys[:]
    # we want to go from left to right...to work with pop on regular list we
    # have to reverse the order.
    keys_copy.reverse()

    # walk down the config tree to the bottom level
    area = context.config
    while len(keys_copy) > 1:
        key = keys_copy.pop()
        if key in area:
            area = area[key]
        else:
            chain = '.'.join(keys[:keys.index(key)+1])
            raise ReflektorCLIError(
                "Could not find \"{!s}\" in config.".format(chain)
            )

    # save the value if it is a valid key
    last_key = keys_copy.pop()
    if last_key in area:
        area[last_key] = value
    else:
        raise ReflektorCLIError(
            "Could not find \"{!s}\" in config.".format('.'.join(keys))
        )

    context.config.save()


@config.command(short_help="Edit the config file.")
def edit():
    """
    Open your default editor and edit the config file.
    """
    click.edit(filename=context.config.path)


@config.command(short_help="Show the config.")
def show():
    """
    Print the reflektor YAML representation of the terminal.
    """
    click.echo()
    click.secho(context.config.path, fg="green")
    click.echo()
    click.secho(context.config.show(), fg="yellow")

