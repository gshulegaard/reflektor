# -*- coding: utf-8 -*-
import click

from reflektor.context import context
from reflektor.cli.config import config


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
def main():
    """
    refLektor CLI
    """
    pass


main.add_command(config)

