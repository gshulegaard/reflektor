# -*- coding: utf-8 -*-
from click import ClickException

from reflektor.common.error import ReflektorError


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


class ReflektorCLIError(ReflektorError, ClickException):
    description = "An error occurred in the CLI."
    exit_code = 1

