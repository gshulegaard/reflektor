# -*- coding: utf-8 -*-
import os
import time
from itertools import cycle

from reflektor.util.singleton import Singleton


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


class Context(Singleton):
    def __init__(self):
        if not self._init:
            return

        # constant vars
        self.pid = os.getpid()
        self.config = None

        # logs
        self.default_log = None
        self.error_log = None

        # dynamic vars
        self.ids = cycle(range(10000, 1000000))
        self.action_stamp = 0

        # environment ('production', 'development', 'testing')
        self.environment = os.environ.get('REFLEKTOR_ENV', 'production')
        self.config_dir = '/etc/reflektor/' \
            if self.environment not in ('development', 'testing') \
            else 'etc/'

        self.setup()

        super().__init__()  # disable future init calls

    def setup(self):
        self._setup_app_config()
        self._setup_app_logs()

    def _setup_app_config(self):
        """
        Loads and sets up app config
        """
        from reflektor.common.config import ReflektorConfig
        fname = 'reflektor.yaml' if self.environment == 'production' \
            else '{!s}.yaml'.format(self.environment)
        self.config = ReflektorConfig(self.config_dir + fname)

    def _setup_app_logs(self):
        """
        Setup app logs
        """
        from reflektor.common import logger
        logger.setup(self.config.logging_config)
        self.default_log = logger.get('reflektor-default')
        self.error_log = logger.get('reflektor-error')

    def inc_action_id(self):
        self.action_stamp = time.time()
        self.action_id = '{!s}_{!s}'.format(self.pid, next(self.ids))

    @property
    def action_duration(self):
        return time.time() - self.action_stamp


context = Context()

