#! /usr/bin/python3
# -*- coding: utf-8 -*-
import click

# add reflektor to sys.path
import os
import sys
sys.path.insert(0, os.getcwd())

from reflektor.util import subp
from reflektor.cli import ReflektorCLIError


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"



WARNING = """
    This script requires `sudo` in order to modify kernal user and group
    mappings.  This is required in order to map container user and groups to
    the current user so that LXD mounting of host file systems can work as
    desired and in a safe(ish) manner.

    For more information consult strgraber's post:

        https://stgraber.org/2017/06/15/custom-user-mappings-in-lxd-containers/

    Also of possible interest:

        https://github.com/lxc/lxd/issues/733
"""


# Script

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
def main():
    """
    Helper script for configuring user/group LXD mappings.  Requires `sudo`.
    """
    # Get user permission to continue
    click.secho(WARNING, fg='yellow')
    user_continue = click.prompt('Continue?', default='n')[0].lower()
    if user_continue != 'y':
        return 0  # Exit without a failure code

    # Map user
    subp.call(
        'printf "lxd:$(id -u):1\nroot:$(id -u):1\n" | sudo tee -a /etc/subuid'
    )

    # Map group
    subp.call(
        'printf "lxd:$(id -g):1\nroot:$(id -g):1\n" | sudo tee -a /etc/subgid'
    )

    # Restart lxd
    subp.call(
        'sudo snap restart lxd'
    )


if __name__ == '__main__':
    main()

