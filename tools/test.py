#! /usr/bin/python3
# -*- coding: utf-8 -*-
import time
import click

# add base repo directory to sys.path
import os
import sys
sys.path.insert(0, os.getcwd())

from reflektor.util import subp
from reflektor.cli import ReflektorError

from tools.helpers import failed_exit


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


# Globals

CNAME = "reflektor-test"
CDEVNAME = "reflektor-mnt"
CMNTPATH = "/root/reflektor"
CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


# Helpers

def container_exec(cmd):
    return subp.call(
        f"lxc exec {CNAME} -- sh -c '{cmd}'"
    )


def build_container():
    # Launch a new container
    click.secho(
        'Launching new 18.04 container ... ',
        fg='yellow',
        nl=False
    )
    try:
        subp.call(f'lxc launch ubuntu:18.04 {CNAME}')
    except subp.ReflektorSubpError:
        failed_exit()
    else:
        click.secho('Success', fg='green')

    # Add the cwd to the container (as the default root directory)
    click.secho(
        'Add local directory to container ... ',
        fg='yellow',
        nl=False
    )
    try:
        subp.call(
            f'lxc config device add {CNAME} {CDEVNAME} '
            f'disk source=$(pwd) path={CMNTPATH}'
        )
    except subp.ReflektorSubpError:
        failed_exit()
    else:
        click.secho('Success', fg='green')

    # Use custom uid and gid map for container
    click.secho(
        'Adding custom uid and gid map to container ... ',
        fg='yellow',
        nl=False
    )
    try:
        subp.call(
            'printf "uid $(id -u) 1000\ngid $(id -g) 1000" '
            f'| lxc config set {CNAME} raw.idmap -'
        )
    except subp.ReflektorSubpError:
        failed_exit()
    else:
        click.secho('Success', fg='green')


    # Restart the test container
    click.secho(
        'Restarting container ... ',
        fg='yellow',
        nl=False
    )
    try:
        subp.call(f'lxc restart {CNAME}')
        time.sleep(4)  # we need to sleep to let container boot
    except subp.ReflektorSubpError:
        if not mute:
            failed_exit()
    else:
        click.secho('Success', fg='green')

    # Install dependencies
    click.secho(
        'Installing dependencies in the container ... ',
        fg='yellow',
        nl=False
    )
    try:
        container_exec('cd reflektor && tools/install-deps.py')
    except subp.ReflektorSubpError:
        failed_exit()
    else:
        click.secho('Success', fg='green')


def drop_container(mute=False):
    click.secho(
        'Dropping container ... ',
        fg='yellow',
        nl=False
    )
    try:
        subp.call(
            f"lxc delete --force {CNAME}"
        )
    except subp.ReflektorSubpError:
        if not mute:
            failed_exit()

    click.secho('Success', fg='green')


def enter():
    click.secho(
        'Entering test container ... ',
        fg='yellow',
    )
    try:
        subp.call(
            f'lxc exec {CNAME} bash',
            fg=True
        )
    except subp.ReflektorSubpError:
        failed_exit()
    else:
        click.secho('Finished', fg='green')


def start(mute=False):
    click.secho(
        'Starting test container ... ',
        fg='yellow',
        nl=False
    )
    try:
        subp.call(
            f'lxc start {CNAME}'
        )
        time.sleep(4)  # wait to let container boot
    except subp.ReflektorSubpError:
        if not mute:
            failed_exit()

    click.secho('Success', fg='green')


def stop(mute=False):
    click.secho(
        'Stopping test container ... ',
        fg='yellow',
        nl=False
    )
    try:
        subp.call(
            f'lxc stop {CNAME}'
        )
    except subp.ReflektorSubpError:
        if not mute:
            failed_exit()

    click.secho('Success', fg='green')


# Commands

@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('--build', '-b', type=bool, is_flag=True, default=False,
              help='Build the test container.  Existing container will be '
                   'dropped if applicable.')
@click.option('--clean', '-c', type=bool, is_flag=True, default=False,
              help='Drop the test container on exit.')
@click.option('--drop', '-d', type=bool, is_flag=True, default=False,
              help='Drop the existing test container and exit (without '
                   'running tests).')
def main(build, clean, drop):
    if drop:
        drop_container()
        return

    if build:
        drop_container(mute=True)
        build_container()
    else:
        start()

    enter()

    stop()

    if clean:
        drop_container()


if __name__ == '__main__':
    main()

