#! /usr/bin/python3
# -*- coding: utf-8 -*-
import subprocess
import click


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


# Helpers

def call(command, stdin=None, check=True, raw=False):
    """
    Calls subprocess.Popen with the command

    :command: full shell command
    :stdin: stdin to passthrough via communicate
    :check: check the return code or not
    :raw: flag to return raw outputs or not
    :return: retcode int, stdout [], stderr []
    """
    subprocess_params = dict(
        shell=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    if stdin:
        del subprocess_params['stdin']
        subprocess_params['input'] = stdin

    process = subprocess.run(command, **subprocess_params)
    try:
        if process.returncode != 0 and check:
            raise Exception(
                message="'{0!s}' returned '{1!s}'".format(
                    command, process.returncode
                )
            )
        elif raw:
            return process.returncode, process.stdout, process.stderr
        else:
            # out, err are bytes objects
            out = process.stdout.decode().split('\n')
            err = process.stderr.decode().split('\n')
            return process.returncode, out, err
    except:
        raise


# Script

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
def main():
    """
    Script that will use `apt` to install reflektor dependencies.
    """
    call('apt update')
    call('apt install -y make git nginx python3-pip')
    # TODO: Should probably download requirements from somewhere first.
    call('pip3 install -r test-requirements.txt')


if __name__ == '__main__':
    main()

