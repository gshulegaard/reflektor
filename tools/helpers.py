# -*- coding: utf-8 -*-
import click

from reflektor.cli import ReflektorCLIError


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


def failed_exit():
    click.secho('Failed', fg='red')
    click.echo()
    click.secho('Exiting prematurely due to failure.', fg='red')
    click.echo()
    import traceback
    traceback.print_exc()
    raise ReflektorCLIError()

