# -*- coding: utf-8 -*-
import os
from tests import here as test_dir
from tests.base import BaseTestCase

from reflektor.context import context
from reflektor.util import subp
from reflektor import git


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


class GitTestCase(BaseTestCase):
    def setup_method(self, method):
        super().setup_method(method)
        self.teardown_actions = []

    def teardown_method(self, method):
        for action in self.teardown_actions:
            action()
        super().teardown_method(method)

    def test_format_url(self):
        # default (basic)
        url = git._format_url()
        assert url == 'https://git@None'

        # proper token
        context.config['git']['repo'] = 'gitlab.com/me/repo.git'
        context.config['git']['credentials'] = 'my-secret-token'
        context.config['git']['auth'] = 'token'
        url = git._format_url()
        assert url == 'https://oauth2:my-secret-token@gitlab.com/me/repo.git'

        # proper ssh
        context.config['git']['repo'] = 'gitlab.com/me/repo.git'
        context.config['git']['auth'] = 'ssh'
        url = git._format_url()
        assert url == 'ssh://git@gitlab.com/me/repo.git'

        # proper basic
        context.config['git']['repo'] = 'gitlab.com/me/repo.git'
        context.config['git']['auth'] = 'basic'
        context.config['git']['credentials'] = 'me:mypassword'
        url = git._format_url()
        assert url == 'https://me:mypassword@gitlab.com/me/repo.git'

    def test_clone(self):
        # add a removal to the teardown logic
        self.teardown_actions.append(git.remove)

        # assert that the target directory is not there
        git_target = git.PATH.split('/')[-1]
        dirs = [dir for dir in os.listdir('.') if not dir.startswith('.')]
        assert git_target not in dirs

        # set git config
        context.config['git']['repo'] = 'gitlab.com/gshulegaard/my-emacs.git'

        git.clone()

        # check that clone made the new directory
        dirs = [dir for dir in os.listdir('.') if not dir.startswith('.')]
        assert git_target in dirs

        # check for README
        assert os.path.isfile(git.PATH + '/README.md')

    def test_remove(self):
        # add removal to teardown as cleanup
        self.teardown_actions.append(git.remove)

        # create feaux target directory
        git_target = git.PATH.split('/')[-1]
        subp.call('mkdir {!s}'.format(git_target))
        dirs = [dir for dir in os.listdir('.') if not dir.startswith('.')]
        assert git_target in dirs

        git.remove()

        # check that dir was removed
        dirs = [dir for dir in os.listdir('.') if not dir.startswith('.')]
        assert git_target not in dirs

    def test_set_remote_origin(self):
        self.teardown_actions.append(git.remove)

        cwd = os.getcwd()
        self.teardown_actions.append(lambda: subp.call('cd {!s}'.format(cwd)))

        context.config['git']['repo'] = 'gitlab.com/gshulegaard/my-emacs.git'

        git.clone()

        # change config so set_remote_origin will actually change something
        context.config['git']['repo'] = 'gitlab.com/gshulegaard/somethingnew.git'
        subp.call('cd {!s}'.format(git.PATH))
        git.set_remote_origin()

        ret_code, out, err = subp.call(
            'cd {!s} && git remote -v'.format(git.PATH)
        )
        assert ret_code == 0
        for line in out:
            if line:  # if line is not empty
                assert git._format_url() in line
                assert 'gitlab.com/gshulegaard/my-emacs.git' not in line

    def test_pull(self):
        """
        Pull is fault tolerant...so it is a bit more difficult to check without
        doing some creative mocking.  For now, just call it after a normal flow
        and make sure no error is raised.
        """
        self.teardown_actions.append(git.remove)

        context.config['git']['repo'] = 'gitlab.com/gshulegaard/my-emacs.git'
        git.clone()
        git.set_remote_origin()
        git.pull()

