# -*- coding: utf-8 -*-
import copy
import unittest

from reflektor.context import context


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


class BaseTestCase(unittest.TestCase):
    @classmethod
    def setup_class(cls):
        context.setup()

    def setup_method(self, method):
        context.inc_action_id()
        context.default_log.info(
            '%s %s::%s %s' % ('=' * 20, self.__class__.__name__, self._testMethodName, '=' * 20)
        )
        self.saved_config = copy.deepcopy(context.config.config)

    def teardown_method(self, method):
        # restore the config
        context.config.config = self.saved_config
        context.config.save()

