# -*- coding: utf-8 -*-
import pytest


__author__ = "Grant Hulegaard"
__maintainer__ = "Grant Hulegaard"
__email__ = "loki.labrys@gmail.com"


disabled_test = pytest.mark.skipif(1 > 0, reason='This test is disabled.')

