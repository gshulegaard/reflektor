.PHONY: help clean rebuild rebuild-test test test-all test-env
TESTS = "tests/"

help:
	@echo "Please use \`make <target>' where <target> is one of:"
	@echo "  clean               to remove build artifacts."
	@echo "  rebuild-test        remove and recreate all tox virtual environments."
	@echo "  test                to run tests with every python interpreter available."
	@echo "  test-all            to run tests with all required python interpreters."
	@echo "  ENV=\"\$$env\" test-env to run tests with a single interpreter"
	@echo "                      where \$$env is one of: py36, py37"
	@echo ""
	@echo "All test recipes can specify a subset of tests using the "
	@echo "\"TESTS\" macro with a py.test target form.  E.G:"
	@echo ""
	@echo "  \$$ make TESTS=\"tests/<file>.py[::test]\" test"
	@echo ""

clean:
	@rm -fr 'dist/'
	@rm -fr 'build/'
	@find . -path '*/.*' -prune -o -name '__pycache__' -exec rm -fr {} +
	@find . -path '*/.*' -prune -o -name '*.egg-info' -exec rm -fr {} +
	@find . -path '*/.*' -prune -o -name '*.py[co]' -exec rm -fr {} +
	@find . -path '*/.*' -prune -o -name '*.build' -exec rm -fr {} +
	@find . -path '*/.*' -prune -o -name '*.so' -exec rm -fr {} +
	@find . -path '*/.*' -prune -o -name '*.c' -exec rm -fr {} +
	@find . -path '*/.*' -prune -o -name '*~' -exec rm -fr {} +

rebuild-test: clean
	rm -fr .tox
	python3 -m tox --skip-missing-interpreters --recreate --notest
	@make clean

test: clean
	python3 -m tox --skip-missing-interpreters -- ${TESTS}
	@make clean

test-all: clean
	python3 -m tox -- ${TESTS}
	@make clean

test-env: clean
	python3 -m tox -e ${ENV} -- ${TESTS}
	@make clean
